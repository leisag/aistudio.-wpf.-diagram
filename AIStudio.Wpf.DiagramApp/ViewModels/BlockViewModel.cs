﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using AIStudio.Wpf.Block;
using AIStudio.Wpf.Block.Core.ViewModels;
using AIStudio.Wpf.DiagramApp.Models;
using AIStudio.Wpf.DiagramDesigner;

namespace AIStudio.Wpf.DiagramApp.ViewModels
{
    internal class BlockViewModel : PageViewModel
    {
        public BlockViewModel(string title, string status, DiagramType diagramType) : base(title, status, diagramType)
        {

        }
        public BlockViewModel(string filename, DiagramDocument diagramDocument) : base(filename, diagramDocument)
        {
            
        }

        protected override void InitDiagramViewModel()
        {
            base.InitDiagramViewModel();

            DiagramViewModel.DiagramOption.LayoutOption.PageSizeOrientation = PageSizeOrientation.Horizontal;
            DiagramViewModel.DiagramOption.LayoutOption.ShowGrid = false;
        }

        protected override void Init(bool initNew)
        {
            base.Init(initNew);

            #region
            //运动
            BlockDesignerItemViewModel MoveNode = new MoveNode(DiagramViewModel) { Name = nameof(MoveNode), Left = 28, Top = 28 };
            DiagramViewModel.Add(MoveNode);
            BlockDesignerItemViewModel TurnRightNode = new TurnRightNode(DiagramViewModel) { Name = nameof(TurnRightNode), Left = 28, Top = 68 };
            DiagramViewModel.Add(TurnRightNode);
            BlockDesignerItemViewModel TurnLeftNode = new TurnLeftNode(DiagramViewModel) { Name = nameof(TurnLeftNode), Left = 28, Top = 108 };
            DiagramViewModel.Add(TurnLeftNode);
            BlockDesignerItemViewModel MoveToNode = new MoveToNode(DiagramViewModel) { Name = nameof(MoveToNode), Left = 28, Top = 148 };
            DiagramViewModel.Add(MoveToNode);
            BlockDesignerItemViewModel MoveToPointNode = new MoveToPointNode(DiagramViewModel) { Name = nameof(MoveToPointNode), Left = 28, Top = 188 };
            DiagramViewModel.Add(MoveToPointNode);
            BlockDesignerItemViewModel TimeMoveToNode = new TimeMoveToNode(DiagramViewModel) { Name = nameof(TimeMoveToNode), Left = 28, Top = 228 };
            DiagramViewModel.Add(TimeMoveToNode);
            BlockDesignerItemViewModel TimeMoveToPointNode = new TimeMoveToPointNode(DiagramViewModel) { Name = nameof(TimeMoveToPointNode), Left = 28, Top = 268 };
            DiagramViewModel.Add(TimeMoveToPointNode);
            BlockDesignerItemViewModel FaceToNode = new FaceToNode(DiagramViewModel) { Name = nameof(FaceToNode), Left = 28, Top = 308 };
            DiagramViewModel.Add(FaceToNode);
            BlockDesignerItemViewModel FaceToMouseNode = new FaceToMouseNode(DiagramViewModel) { Name = nameof(FaceToMouseNode), Left = 28, Top = 348 };
            DiagramViewModel.Add(FaceToMouseNode);
            BlockDesignerItemViewModel PointXAddNode = new PointXAddNode(DiagramViewModel) { Name = nameof(PointXAddNode), Left = 28, Top = 388 };
            DiagramViewModel.Add(PointXAddNode);
            BlockDesignerItemViewModel PointXSetNode = new PointXSetNode(DiagramViewModel) { Name = nameof(PointXSetNode), Left = 28, Top = 428 };
            DiagramViewModel.Add(PointXSetNode);
            BlockDesignerItemViewModel PointYAddNode = new PointYAddNode(DiagramViewModel) { Name = nameof(PointYAddNode), Left = 28, Top = 468 };
            DiagramViewModel.Add(PointYAddNode);
            BlockDesignerItemViewModel PointYSetNode = new PointYSetNode(DiagramViewModel) { Name = nameof(PointYSetNode), Left = 28, Top = 508 };
            DiagramViewModel.Add(PointYSetNode);
            BlockDesignerItemViewModel BorderBounceNode = new BorderBounceNode(DiagramViewModel) { Name = nameof(BorderBounceNode), Left = 28, Top = 548 };
            DiagramViewModel.Add(BorderBounceNode);
            BlockDesignerItemViewModel RotationMethodNode = new RotationMethodNode(DiagramViewModel) { Name = nameof(RotationMethodNode), Left = 28, Top = 588 };
            DiagramViewModel.Add(RotationMethodNode);
            BlockDesignerItemViewModel PointXNode = new PointXNode(DiagramViewModel) { Name = nameof(PointXNode), Left = 28, Top = 628 };
            DiagramViewModel.Add(PointXNode);
            BlockDesignerItemViewModel PointYNode = new PointYNode(DiagramViewModel) { Name = nameof(PointYNode), Left = 28, Top = 668 };
            DiagramViewModel.Add(PointYNode);
            BlockDesignerItemViewModel DirectionNode = new DirectionNode(DiagramViewModel) { Name = nameof(DirectionNode), Left = 28, Top = 708 };
            DiagramViewModel.Add(DirectionNode);

            BlockDesignerItemViewModel SayTimeNode = new SayTimeNode(DiagramViewModel) { Name = nameof(SayTimeNode), Left = 288, Top = 28 };
            DiagramViewModel.Add(SayTimeNode);
            BlockDesignerItemViewModel SayNode = new SayNode(DiagramViewModel) { Name = nameof(SayNode), Left = 288, Top = 68 };
            DiagramViewModel.Add(SayNode);
            BlockDesignerItemViewModel ThinkTimeNode = new ThinkTimeNode(DiagramViewModel) { Name = nameof(ThinkTimeNode), Left = 288, Top = 108 };
            DiagramViewModel.Add(ThinkTimeNode);
            BlockDesignerItemViewModel ThinkBlockNode = new ThinkBlockNode(DiagramViewModel) { Name = nameof(ThinkBlockNode), Left = 288, Top = 148 };
            DiagramViewModel.Add(ThinkBlockNode);
            BlockDesignerItemViewModel ChangeLookNode = new ChangeLookNode(DiagramViewModel) { Name = nameof(ChangeLookNode), Left = 288, Top = 188 };
            DiagramViewModel.Add(ChangeLookNode);
            BlockDesignerItemViewModel NextLookNode = new NextLookNode(DiagramViewModel) { Name = nameof(NextLookNode), Left = 288, Top = 228 };
            DiagramViewModel.Add(NextLookNode);
            BlockDesignerItemViewModel ChangeBackgroundNode = new ChangeBackgroundNode(DiagramViewModel) { Name = nameof(ChangeBackgroundNode), Left = 288, Top = 268 };
            DiagramViewModel.Add(ChangeBackgroundNode);
            BlockDesignerItemViewModel NextBackgroundNode = new NextBackgroundNode(DiagramViewModel) { Name = nameof(NextBackgroundNode), Left = 288, Top = 308 };
            DiagramViewModel.Add(NextBackgroundNode);
            BlockDesignerItemViewModel AddSizeNode = new AddSizeNode(DiagramViewModel) { Name = nameof(AddSizeNode), Left = 288, Top = 348 };
            DiagramViewModel.Add(AddSizeNode);
            BlockDesignerItemViewModel SetSizeNode = new SetSizeNode(DiagramViewModel) { Name = nameof(SetSizeNode), Left = 288, Top = 388 };
            DiagramViewModel.Add(SetSizeNode);
            BlockDesignerItemViewModel ClearEffectNode = new ClearEffectNode(DiagramViewModel) { Name = nameof(ClearEffectNode), Left = 288, Top = 428 };
            DiagramViewModel.Add(ClearEffectNode);
            BlockDesignerItemViewModel ShowEffectNode = new ShowNode(DiagramViewModel) { Name = nameof(ShowNode), Left = 288, Top = 468 };
            DiagramViewModel.Add(ShowEffectNode);
            BlockDesignerItemViewModel HideEffectNode = new HideNode(DiagramViewModel) { Name = nameof(HideNode), Left = 288, Top = 508 };
            DiagramViewModel.Add(HideEffectNode);
            BlockDesignerItemViewModel BringToNode = new BringToNode(DiagramViewModel) { Name = nameof(BringToNode), Left = 288, Top = 548 };
            DiagramViewModel.Add(BringToNode);
            BlockDesignerItemViewModel BringLayerNode = new BringLayerNode(DiagramViewModel) { Name = nameof(BringLayerNode), Left = 288, Top = 588 };
            DiagramViewModel.Add(BringLayerNode);
            BlockDesignerItemViewModel LookNode = new LookNode(DiagramViewModel) { Name = nameof(LookNode), Left = 288, Top = 628 };
            DiagramViewModel.Add(LookNode);
            BlockDesignerItemViewModel BackgroundNode = new BackgroundNode(DiagramViewModel) { Name = nameof(BackgroundNode), Left = 288, Top = 668 };
            DiagramViewModel.Add(BackgroundNode);
            BlockDesignerItemViewModel SizeNode = new SizeNode(DiagramViewModel) { Name = nameof(SizeNode), Left = 288, Top = 708 };
            DiagramViewModel.Add(SizeNode);

            BlockDesignerItemViewModel PlaySoundCompleteNode = new PlaySoundCompleteNode(DiagramViewModel) { Name = nameof(PlaySoundCompleteNode), Left = 448, Top = 28 };
            DiagramViewModel.Add(PlaySoundCompleteNode);
            BlockDesignerItemViewModel PlaySoundNode = new PlaySoundNode(DiagramViewModel) { Name = nameof(PlaySoundNode), Left = 448, Top = 68 };
            DiagramViewModel.Add(PlaySoundNode);
            BlockDesignerItemViewModel StopAllSoundNode = new StopAllSoundNode(DiagramViewModel) { Name = nameof(StopAllSoundNode), Left = 448, Top = 108 };
            DiagramViewModel.Add(StopAllSoundNode);
            BlockDesignerItemViewModel AddSoundEffectNode = new AddSoundEffectNode(DiagramViewModel) { Name = nameof(AddSoundEffectNode), Left = 448, Top = 148 };
            DiagramViewModel.Add(AddSoundEffectNode);
            BlockDesignerItemViewModel SetSoundEffectNode = new SetSoundEffectNode(DiagramViewModel) { Name = nameof(SetSoundEffectNode), Left = 448, Top = 188 };
            DiagramViewModel.Add(SetSoundEffectNode);
            BlockDesignerItemViewModel ClearSoundEffectNode = new ClearSoundEffectNode(DiagramViewModel) { Name = nameof(ClearSoundEffectNode), Left = 448, Top = 228 };
            DiagramViewModel.Add(ClearSoundEffectNode);
            BlockDesignerItemViewModel AddVolumeNode = new AddVolumeNode(DiagramViewModel) { Name = nameof(AddVolumeNode), Left = 448, Top = 268 };
            DiagramViewModel.Add(AddVolumeNode);
            BlockDesignerItemViewModel SetVolumeNode = new SetVolumeNode(DiagramViewModel) { Name = nameof(SetVolumeNode), Left = 448, Top = 308 };
            DiagramViewModel.Add(SetVolumeNode);
            BlockDesignerItemViewModel VolumeNode = new VolumeNode(DiagramViewModel) { Name = nameof(VolumeNode), Left = 448, Top = 348 };
            DiagramViewModel.Add(VolumeNode);

            BlockDesignerItemViewModel StartNode = new StartNode(DiagramViewModel) { Name = nameof(StartNode), Left = 638, Top = 28 };
            DiagramViewModel.Add(StartNode);
            BlockDesignerItemViewModel KeyboardPressNode = new KeyboardPressNode(DiagramViewModel) { Name = nameof(KeyboardPressNode), Left = 638, Top = 88 };
            DiagramViewModel.Add(KeyboardPressNode);
            BlockDesignerItemViewModel WhenRoleClickedNode = new WhenRoleClickedNode(DiagramViewModel) { Name = nameof(WhenRoleClickedNode), Left = 638, Top = 148 };
            DiagramViewModel.Add(WhenRoleClickedNode);
            BlockDesignerItemViewModel WhenBackgroundSwitchNode = new WhenBackgroundSwitchNode(DiagramViewModel) { Name = nameof(WhenBackgroundSwitchNode), Left = 638, Top = 208 };
            DiagramViewModel.Add(WhenBackgroundSwitchNode);
            BlockDesignerItemViewModel WhenGreaterThanNode = new WhenGreaterThanNode(DiagramViewModel) { Name = nameof(WhenGreaterThanNode), Left = 638, Top = 268 };
            DiagramViewModel.Add(WhenGreaterThanNode);
            BlockDesignerItemViewModel WhenReceivedMessageNode = new WhenReceivedMessageNode(DiagramViewModel) { Name = nameof(WhenReceivedMessageNode), Left = 638, Top = 328 };
            DiagramViewModel.Add(WhenReceivedMessageNode);
            BlockDesignerItemViewModel BroadcastMessageNode = new BroadcastMessageNode(DiagramViewModel) { Name = nameof(BroadcastMessageNode), Left = 638, Top = 388 };
            DiagramViewModel.Add(BroadcastMessageNode);
            BlockDesignerItemViewModel BroadcastMessageAndWaitingNode = new BroadcastMessageAndWaitingNode(DiagramViewModel) { Name = nameof(BroadcastMessageAndWaitingNode), Left = 638, Top = 448 };
            DiagramViewModel.Add(BroadcastMessageAndWaitingNode);

            BlockDesignerItemViewModel WaitTimeNode = new WaitTimeNode(DiagramViewModel) { Name = nameof(WaitTimeNode), Left = 788, Top = 28 };
            DiagramViewModel.Add(WaitTimeNode);
            BlockDesignerItemViewModel ForNode = new ForNode(DiagramViewModel) { Name = nameof(ForNode), Left = 788, Top = 68 };
            DiagramViewModel.Add(ForNode);
            BlockDesignerItemViewModel AlwaysNode = new AlwaysNode(DiagramViewModel) { Name = nameof(AlwaysNode), Left = 788, Top = 158 };
            DiagramViewModel.Add(AlwaysNode);
            BlockDesignerItemViewModel IfNode = new IfNode(DiagramViewModel) { Name = nameof(IfNode), Left = 788, Top = 238 };
            DiagramViewModel.Add(IfNode);
            BlockDesignerItemViewModel IfElseNode = new IfElseNode(DiagramViewModel) { Name = nameof(IfElseNode), Left = 788, Top = 328 };
            DiagramViewModel.Add(IfElseNode);
            BlockDesignerItemViewModel WaitNode = new WaitNode(DiagramViewModel) { Name = nameof(WaitNode), Left = 788, Top = 458 };
            DiagramViewModel.Add(WaitNode);
            BlockDesignerItemViewModel WhileNode = new WhileNode(DiagramViewModel) { Name = nameof(WhileNode), Left = 788, Top = 498 };
            DiagramViewModel.Add(WhileNode);
            BlockDesignerItemViewModel StopNode = new StopNode(DiagramViewModel) { Name = nameof(StopNode), Left = 788, Top = 578 };
            DiagramViewModel.Add(StopNode);
            BlockDesignerItemViewModel CloneStartNode = new CloneStartNode(DiagramViewModel) { Name = nameof(CloneStartNode), Left = 788, Top = 618 };
            DiagramViewModel.Add(CloneStartNode);
            BlockDesignerItemViewModel CloneNode = new CloneNode(DiagramViewModel) { Name = nameof(CloneNode), Left = 788, Top = 678 };
            DiagramViewModel.Add(CloneNode);
            BlockDesignerItemViewModel DeleteThisCloneNode = new DeleteThisCloneNode(DiagramViewModel) { Name = nameof(DeleteThisCloneNode), Left = 788, Top = 718 };
            DiagramViewModel.Add(DeleteThisCloneNode);

            BlockDesignerItemViewModel HitNode = new HitNode(DiagramViewModel) { Name = nameof(HitNode), Left = 928, Top = 28 };
            DiagramViewModel.Add(HitNode);
            BlockDesignerItemViewModel HitColorNode = new HitColorNode(DiagramViewModel) { Name = nameof(HitColorNode), Left = 928, Top = 68 };
            DiagramViewModel.Add(HitColorNode);
            BlockDesignerItemViewModel ColorHitColorNode = new ColorHitColorNode(DiagramViewModel) { Name = nameof(ColorHitColorNode), Left = 928, Top = 108 };
            DiagramViewModel.Add(ColorHitColorNode);
            BlockDesignerItemViewModel DistanceNode = new DistanceNode(DiagramViewModel) { Name = nameof(DistanceNode), Left = 928, Top = 148 };
            DiagramViewModel.Add(DistanceNode);
            BlockDesignerItemViewModel AskNode = new AskNode(DiagramViewModel) { Name = nameof(AskNode), Left = 928, Top = 188 };
            DiagramViewModel.Add(AskNode);
            BlockDesignerItemViewModel AnswerNode = new AnswerNode(DiagramViewModel) { Name = nameof(AnswerNode), Left = 928, Top = 228 };
            DiagramViewModel.Add(AnswerNode);
            BlockDesignerItemViewModel PressKeyNode = new PressKeyNode(DiagramViewModel) { Name = nameof(PressKeyNode), Left = 928, Top = 268 };
            DiagramViewModel.Add(PressKeyNode);
            BlockDesignerItemViewModel PressMouseNode = new PressMouseNode(DiagramViewModel) { Name = nameof(PressMouseNode), Left = 928, Top = 308 };
            DiagramViewModel.Add(PressMouseNode);
            BlockDesignerItemViewModel MousePointXNode = new MousePointXNode(DiagramViewModel) { Name = nameof(MousePointXNode), Left = 928, Top = 348 };
            DiagramViewModel.Add(MousePointXNode);
            BlockDesignerItemViewModel MousePointYNode = new MousePointYNode(DiagramViewModel) { Name = nameof(MousePointYNode), Left = 928, Top = 388 };
            DiagramViewModel.Add(MousePointYNode);
            BlockDesignerItemViewModel DragModeNode = new DragModeNode(DiagramViewModel) { Name = nameof(DragModeNode), Left = 928, Top = 428 };
            DiagramViewModel.Add(DragModeNode);
            BlockDesignerItemViewModel LoudnessNode = new LoudnessNode(DiagramViewModel) { Name = nameof(LoudnessNode), Left = 928, Top = 468 };
            DiagramViewModel.Add(LoudnessNode);
            BlockDesignerItemViewModel TimerNode = new TimerNode(DiagramViewModel) { Name = nameof(TimerNode), Left = 928, Top = 508 };
            DiagramViewModel.Add(TimerNode);
            BlockDesignerItemViewModel ClearTimerNode = new ClearTimerNode(DiagramViewModel) { Name = nameof(ClearTimerNode), Left = 928, Top = 548 };
            DiagramViewModel.Add(ClearTimerNode);
            BlockDesignerItemViewModel StageObjectNode = new StageObjectNode(DiagramViewModel) { Name = nameof(StageObjectNode), Left = 928, Top = 588 };
            DiagramViewModel.Add(StageObjectNode);
            BlockDesignerItemViewModel NowTimeNode = new NowTimeNode(DiagramViewModel) { Name = nameof(NowTimeNode), Left = 928, Top = 628 };
            DiagramViewModel.Add(NowTimeNode);
            BlockDesignerItemViewModel DaysFrom2000Node = new DaysFrom2000Node(DiagramViewModel) { Name = nameof(DaysFrom2000Node), Left = 928, Top = 668 };
            DiagramViewModel.Add(DaysFrom2000Node);
            BlockDesignerItemViewModel UserNameNode = new UserNameNode(DiagramViewModel) { Name = nameof(UserNameNode), Left = 928, Top = 708 };
            DiagramViewModel.Add(UserNameNode);

            BlockDesignerItemViewModel AddNode = new AddNode(DiagramViewModel) { Name = nameof(AddNode), Left = 1108, Top = 28 };
            DiagramViewModel.Add(AddNode);
            BlockDesignerItemViewModel SubtractNode = new SubtractNode(DiagramViewModel) { Name = nameof(SubtractNode), Left = 1108, Top = 68 };
            DiagramViewModel.Add(SubtractNode);
            BlockDesignerItemViewModel MultiplyNode = new MultiplyNode(DiagramViewModel) { Name = nameof(MultiplyNode), Left = 1108, Top = 108 };
            DiagramViewModel.Add(MultiplyNode);
            BlockDesignerItemViewModel DivisionNode = new DivisionNode(DiagramViewModel) { Name = nameof(DivisionNode), Left = 1108, Top = 148 };
            DiagramViewModel.Add(DivisionNode);
            BlockDesignerItemViewModel RandomNode = new RandomNode(DiagramViewModel) { Name = nameof(RandomNode), Left = 1108, Top = 188 };
            DiagramViewModel.Add(RandomNode);
            BlockDesignerItemViewModel GreaterThanNode = new GreaterThanNode(DiagramViewModel) { Name = nameof(GreaterThanNode), Left = 1108, Top = 228 };
            DiagramViewModel.Add(GreaterThanNode);
            BlockDesignerItemViewModel LessThanNode = new LessThanNode(DiagramViewModel) { Name = nameof(LessThanNode), Left = 1108, Top = 268 };
            DiagramViewModel.Add(LessThanNode);
            BlockDesignerItemViewModel EqualThanNode = new EqualThanNode(DiagramViewModel) { Name = nameof(EqualThanNode), Left = 1108, Top = 308 };
            DiagramViewModel.Add(EqualThanNode);
            BlockDesignerItemViewModel AndNode = new AndNode(DiagramViewModel) { Name = nameof(AndNode), Left = 1108, Top = 348 };
            DiagramViewModel.Add(AndNode);
            BlockDesignerItemViewModel OrNode = new OrNode(DiagramViewModel) { Name = nameof(OrNode), Left = 1108, Top = 388 };
            DiagramViewModel.Add(OrNode);
            BlockDesignerItemViewModel NotNode = new NotNode(DiagramViewModel) { Name = nameof(NotNode), Left = 1108, Top = 428 };
            DiagramViewModel.Add(NotNode);
            BlockDesignerItemViewModel StringAddNode = new StringAddNode(DiagramViewModel) { Name = nameof(StringAddNode), Left = 1108, Top = 468 };
            DiagramViewModel.Add(StringAddNode);
            BlockDesignerItemViewModel StringIndexNode = new StringIndexNode(DiagramViewModel) { Name = nameof(StringIndexNode), Left = 1108, Top = 508 };
            DiagramViewModel.Add(StringIndexNode);
            BlockDesignerItemViewModel StringContainNode = new StringContainNode(DiagramViewModel) { Name = nameof(StringContainNode), Left = 1108, Top = 548 };
            DiagramViewModel.Add(StringContainNode);
            BlockDesignerItemViewModel ModNode = new ModNode(DiagramViewModel) { Name = nameof(ModNode), Left = 1108, Top = 588 };
            DiagramViewModel.Add(ModNode);
            BlockDesignerItemViewModel RoundNode = new RoundNode(DiagramViewModel) { Name = nameof(RoundNode), Left = 1108, Top = 628 };
            DiagramViewModel.Add(RoundNode);
            BlockDesignerItemViewModel MathNode = new MathNode(DiagramViewModel) { Name = nameof(MathNode), Left = 1108, Top = 668 };
            DiagramViewModel.Add(MathNode);

            BlockDesignerItemViewModel VariableNode = new VariableNode(DiagramViewModel) { Name = nameof(VariableNode), Left = 1318, Top = 28 };
            DiagramViewModel.Add(VariableNode);
            BlockDesignerItemViewModel SetVariableNode = new SetVariableNode(DiagramViewModel) { Name = nameof(SetVariableNode), Left = 1318, Top = 68 };
            DiagramViewModel.Add(SetVariableNode);
            BlockDesignerItemViewModel IncreaseVariableNode = new IncreaseVariableNode(DiagramViewModel) { Name = nameof(IncreaseVariableNode), Left = 1318, Top = 108 };
            DiagramViewModel.Add(IncreaseVariableNode);
            BlockDesignerItemViewModel ShowVariableNode = new ShowVariableNode(DiagramViewModel) { Name = nameof(ShowVariableNode), Left = 1318, Top = 148 };
            DiagramViewModel.Add(ShowVariableNode);
            BlockDesignerItemViewModel HiddenVariableNode = new HiddenVariableNode(DiagramViewModel) { Name = nameof(HiddenVariableNode), Left = 1318, Top = 188 };
            DiagramViewModel.Add(HiddenVariableNode);
            #endregion
        }
    }
}
