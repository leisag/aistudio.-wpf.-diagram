﻿using System;
using System.Collections.Concurrent;
using System.Windows;
using System.Windows.Controls;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class DesignerItemsControlItemStyleSelector : StyleSelector
    {
        static DesignerItemsControlItemStyleSelector()
        {
            Instance = new DesignerItemsControlItemStyleSelector();
        }

        public static DesignerItemsControlItemStyleSelector Instance
        {
            get;
            private set;
        }

        private static ConcurrentDictionary<Type, string> _styles = new ConcurrentDictionary<Type, string>();

        public void RegisterDesignerItemControlItemStyle(Type type, string resourceName)
        {
            if (_styles.ContainsKey(type))
            {
                throw new InvalidOperationException($"{type}-{resourceName} already exists");
            }
            _styles.TryAdd(type, resourceName);
        }

        public void RegisterDesignerItemControlItemStyle(Type type, string resourceName, bool isCover)
        {
            if (_styles.ContainsKey(type))
            {
                _styles[type] = resourceName;
            }
            _styles.TryAdd(type, resourceName);
        }

        public override Style SelectStyle(object item, DependencyObject container)
        {
            ItemsControl itemsControl = ItemsControl.ItemsControlFromItemContainer(container);
            if (itemsControl == null)
                throw new InvalidOperationException("DesignerItemsControlItemStyleSelector : Could not find ItemsControl");

            if (!_styles.IsNullOrEmpty())
            {
                var type = item.GetType();
                if (_styles.ContainsKey(type))
                {
                    return (Style)itemsControl.FindResource(_styles[type]);
                }

                foreach (var styleItem in _styles)
                {
                    if (type.IsSubclassOf(styleItem.Key))
                    {
                        return (Style)itemsControl.FindResource(styleItem.Value);
                    }

                    if (type.IsAssignableFrom(styleItem.Key))
                    {
                        return (Style)itemsControl.FindResource(styleItem.Value);
                    }
                }
            }

            if (item is GifImageItemViewModel)
            {
                return (Style)itemsControl.FindResource("gifimageItemStyle");
            }

            if (item is LogicalGateItemViewModelBase)
            {
                return (Style)itemsControl.FindResource("logicalItemStyle");
            }

            if (item is BlockDesignerItemViewModel)
            {
                return (Style)itemsControl.FindResource("blockItemStyle");
            }

            if (item is DesignerItemViewModelBase)
            {
                return (Style)itemsControl.FindResource("designerItemStyle");
            }

            if (item is ConnectionViewModel)
            {
                return (Style)itemsControl.FindResource("connectorItemStyle");
            }

            return null;
        }
    }
}
