﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public enum PathStyle
    {
        None,
        Arrow,
        Circle,
        Square,       
    }
}
