﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class BlockConnector : ContentControl
    {
        public ConnectorOrientation Orientation { get; set; }

        public ConnectorInfoBase Info
        {
            get
            {
                if (Content is ConnectorInfoBase connectorInfo)
                    return connectorInfo;

                return this.DataContext as ConnectorInfoBase;
            }
        }

    }

    public class BlockGrid : Grid
    {
    }

}
