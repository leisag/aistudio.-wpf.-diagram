﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class ColorBrushConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Brush brush = null;
            if (value is Color)
            {
                brush = new SolidColorBrush((Color)value);
            }
            else if (value is ColorObject colorObject)
            {
                brush = colorObject.ToBrush();
            }
            else if (value is ObservableCollection<GradientStop> gradientStop)
            {
                LinearGradientBrush myBrush = new LinearGradientBrush();
                myBrush.StartPoint = new Point(0, 0.5);
                myBrush.EndPoint = new Point(1, 0.5);
                if (gradientStop != null)
                {
                    foreach (var stop in gradientStop)
                    {
                        myBrush.GradientStops.Add(new System.Windows.Media.GradientStop(stop.Color, stop.Offset));
                    }
                }
                brush = myBrush;
            }

            return brush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
