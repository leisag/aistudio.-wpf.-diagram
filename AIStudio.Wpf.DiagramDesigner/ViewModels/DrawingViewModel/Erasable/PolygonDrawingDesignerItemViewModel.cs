﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class PolygonDrawingDesignerItemViewModel : DrawingDesignerItemViewModelBase
    {
        public PolygonDrawingDesignerItemViewModel()
        {
        }

        public PolygonDrawingDesignerItemViewModel(IDiagramViewModel root, Point startPoint, ColorViewModel colorViewModel, bool erasable) : base(root, DrawMode.ErasableLine, startPoint, colorViewModel, erasable)
        {
        }

        public PolygonDrawingDesignerItemViewModel(IDiagramViewModel root, List<Point> points, ColorViewModel colorViewModel, bool erasable) : base(root, DrawMode.ErasablePolygon, points, colorViewModel, erasable)
        {

        }

        public PolygonDrawingDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public PolygonDrawingDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        protected override void InitNewDrawing()
        {
            if (IsFinish)
            {
                var geometry = new PathGeometry();
                var figure = new PathFigure { StartPoint = Points[0] };
                figure.IsClosed = true;
                geometry.Figures.Add(figure);

                foreach (var point in Points)
                {
                    if (Points[0] == point) continue;

                    LineSegment arc = new LineSegment(point, true) { IsSmoothJoin = true };
                    geometry.Figures[0].Segments.Add(arc);
                }

                Geometry = geometry;
            }
            base.InitNewDrawing();
        }


        public override bool OnMouseMove(IInputElement sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var point = e.GetPosition(sender);
                if (Points == null || Points.Count == 0)//没有起始点
                {
                    return true;
                }

                if ((Points.LastOrDefault() - point).Length < ColorViewModel.LineWidth)
                {
                    return true;
                }


                Points.Add(point);

                if (Geometry is PathGeometry geometry)
                {

                }
                else
                {
                    geometry = new PathGeometry();
                    var figure = new PathFigure { StartPoint = Points[0] };
                    figure.IsClosed = true;
                    geometry.Figures.Add(figure);
                }

                LineSegment arc = new LineSegment(point, true) { IsSmoothJoin = true };
                geometry.Figures[0].Segments.Add(arc);
                Geometry = geometry;

                IsFinish = true;

                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool OnMouseDown(IInputElement sender, MouseButtonEventArgs e)
        {
            return true;
        }

        public override bool OnMouseUp(IInputElement sender, MouseButtonEventArgs e)
        {
            return base.OnMouseUp(sender, e);
        }
    }
}
