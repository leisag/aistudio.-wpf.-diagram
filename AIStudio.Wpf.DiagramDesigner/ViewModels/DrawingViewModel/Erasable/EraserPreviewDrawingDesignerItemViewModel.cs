﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner.Models;
using SvgPathProperties;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class EraserPreviewDrawingDesignerItemViewModel : DrawingDesignerItemViewModelBase
    {
        public EraserPreviewDrawingDesignerItemViewModel()
        {
        }

        public EraserPreviewDrawingDesignerItemViewModel(IDiagramViewModel root, Point startPoint, ColorViewModel colorViewModel) : base(root, DrawMode.Eraser, startPoint, colorViewModel, true)
        {
        }

        public EraserPreviewDrawingDesignerItemViewModel(IDiagramViewModel root, DrawMode drawMode, List<Point> points, ColorViewModel colorViewModel, bool erasable) : base(root, drawMode, points, colorViewModel, erasable)
        {

        }

        public EraserPreviewDrawingDesignerItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public EraserPreviewDrawingDesignerItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override bool OnMouseMove(IInputElement sender, MouseEventArgs e)
        { 
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var point = e.GetPosition(sender);
                if (Points == null || Points.Count == 0)//没有起始点
                {
                    return true;
                }

                if ((Points.LastOrDefault() - point).Length < ColorViewModel.LineWidth)
                {
                    return true;
                }

                Points.Add(point);
                if (Geometry is PathGeometry geometry)
                {

                }
                else
                {
                    geometry = new PathGeometry();
                    var figure = new PathFigure { StartPoint = Points[0] };
                    geometry.Figures.Add(figure);
                }

                LineSegment arc = new LineSegment(point, true) { IsSmoothJoin = true };
                geometry.Figures[0].Segments.Add(arc);
                Geometry = geometry;

                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool OnMouseDown(IInputElement sender, MouseButtonEventArgs e)
        {
            return true;
        }

        public override bool OnMouseUp(IInputElement sender, MouseButtonEventArgs e)
        {
            if (Geometry != null)
            {
                Erase(Geometry);
            }
            return base.OnMouseUp(sender, e);
        }

        private new bool Erase(Geometry geometry)
        {
            var empty = true;
            List<DrawingDesignerItemViewModelBase> deleteDrawGeometries = new List<DrawingDesignerItemViewModelBase>();
            foreach (var g in this.Root.Items.OfType<DrawingDesignerItemViewModelBase>())
            {
                if (g.Erase(geometry.GetWidenedPathGeometry(new Pen(ColorViewModel.LineColor.ToBrush(), ColorViewModel.LineWidth))))
                    deleteDrawGeometries.Add(g);
                else
                    empty = false;
            }

            foreach (var g in deleteDrawGeometries)
            {
                this.Root.Delete(g);
            }

            return empty;
        }
    }
}
