﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Media;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.Mind.Models;
using AIStudio.Wpf.Mind.ViewModels;

namespace AIStudio.Wpf.Mind.Helpers
{
    public interface IMindLayout
    {
        /// <summary>
        ///  默认节点样式设置
        /// </summary>
        /// <param name="mindNode"></param>
        void Appearance(MindNode mindNode);

        /// <summary>
        ///  节点样式设置
        /// </summary>
        /// <param name="mindNode"></param>
        /// <param name="mindTheme"></param>
        /// <param name="initAppearance"></param>
        void Appearance(MindNode mindNode, MindTheme mindTheme, bool initAppearance);

        /// <summary>
        /// 连线类型设置
        /// </summary>
        /// <param name="source"></param>
        /// <param name="sink"></param>
        /// <param name="connector"></param>
        /// <returns></returns>
        ConnectionViewModel GetOrSetConnectionViewModel(MindNode source, MindNode sink, ConnectionViewModel connector = null);

        /// <summary>
        /// 更新布局
        /// </summary>
        /// <param name="mindNode"></param>
        void UpdatedLayout(MindNode mindNode);        
    }
}
