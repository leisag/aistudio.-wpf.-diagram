﻿using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;
using AIStudio.Wpf.Script.Core.ViewModels;

namespace AIStudio.Wpf.Script.ViewModels
{
    public class ColorBoxSwitchScriptViewModel : RoslynScriptViewModel
    {
        public ColorBoxSwitchScriptViewModel()
        {
        }

        public ColorBoxSwitchScriptViewModel(IDiagramViewModel root) : base(root)
        {
        }

        public ColorBoxSwitchScriptViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {
        }

        public ColorBoxSwitchScriptViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {
        }

        protected override void InitNew()
        {
            base.InitNew();

            Text = "ColorBox分配器";
            ItemWidth = 100;
            ItemHeight = 230;
            Code = @"using AIStudio.Wpf.Script.Core.Models;
using System;

namespace AIStudio.Wpf.CSharpScript
{
    public class ColorBoxSwitch
    {
        public bool Mode { private get; set; }
        public string Description { get; private set; }
        public ColorBoxModel Input { private get; set; }
        public ColorBoxModel Output1 { get; private set; }
        public ColorBoxModel Output2 { get; private set; }
        public ColorBoxModel Output3 { get; private set; }
        public ColorBoxModel Output4 { get; private set; }
        public ColorBoxModel Output5 { get; private set; }
        public ColorBoxModel Output6 { get; private set; }
        public ColorBoxModel Output7 { get; private set; }

        public void Execute()
        {
            if (Input == null)
                return;

            if (Mode == false)
            {
                Console.WriteLine($""正在使用按颜色分配，等待分配{Input.Text}号Box"");
                Description = ""欢迎来到AIStudio Diagream Box装配工厂，正在使用按颜色分配Box"";
                switch (Input.Color.ToString())
                {
                    case ""#FFFF0000"":
                        if (Output1 == null)
                        {
                            Output1 = Input;
                            Input = null;
                            Console.WriteLine($""{Output1.Text}号Box分配完毕,送往1号出口"");
                        }
                        break;
                    case ""#FFFFA500"":
                        if (Output2 == null)
                        {
                            Output2 = Input;
                            Input = null;
                            Console.WriteLine($""{Output2.Text}号Box分配完毕,送往2号出口"");
                        }
                        break;
                    case ""#FFFFFF00"":
                        if (Output3 == null)
                        {
                            Output3 = Input;
                            Input = null;
                            Console.WriteLine($""{Output3.Text}号Box分配完毕,送往3号出口"");
                        }
                        break;
                    case ""#FF008000"":
                        if (Output4 == null)
                        {
                            Output4 = Input;
                            Input = null;
                            Console.WriteLine($""{Output4.Text}号Box分配完毕,送往4号出口"");
                        }
                        break;
                    case ""#FF00FFFF"":
                        if (Output5 == null)
                        {
                            Output5 = Input;
                            Input = null;
                            Console.WriteLine($""{Output5.Text}号Box分配完毕,送往5号出口"");
                        }
                        break;
                    case ""#FF0000FF"":
                        if (Output6 == null)
                        {
                            Output6 = Input;
                            Input = null;
                            Console.WriteLine($""{Output6.Text}号Box分配完毕,送往6号出口"");
                        }
                        break;
                    default:
                        if (Output7 == null)
                        {
                            Output7 = Input;
                            Input = null;
                            Console.WriteLine($""{Output7.Text}号Box分配完毕,送往7号出口"");
                        }
                        break;
                }
            }
            else
            {
                Console.WriteLine($""正在使用按形状分配，等待分配{Input.Text}号Box"");
                Description = ""欢迎来到AIStudio Diagream Box装配工厂，正在使用按形状分配Box"";
                switch (Input.Path)
                {
                    case ""M 10,20 A 20,20 0 1 1 50,20 A 20,20 0 1 1 10,20"":
                        if (Output1 == null)
                        {
                            Output1 = Input;
                            Input = null;
                            Console.WriteLine($""{Output1.Text}号Box分配完毕,送往1号出口"");
                        }
                        break;
                    case ""M1,21H23L12,2"":
                        if (Output2 == null)
                        {
                            Output2 = Input;
                            Input = null;
                            Console.WriteLine($""{Output2.Text}号Box分配完毕,送往2号出口"");
                        }
                        break;
                    case ""M3,3V21H21V3"":
                        if (Output3 == null)
                        {
                            Output3 = Input;
                            Input = null;
                            Console.WriteLine($""{Output3.Text}号Box分配完毕,送往3号出口"");
                        }
                        break;
                    case ""M 0,20 L 30 0 L 60,20 L 30,40 Z"":
                        if (Output4 == null)
                        {
                            Output4 = Input;
                            Input = null;
                            Console.WriteLine($""{Output4.Text}号Box分配完毕,送往4号出口"");
                        }
                        break;
                    case ""M10 3.22l-.61-.6a5.5 5.5 0 0 0-7.78 7.77L10 18.78l8.39-8.4a5.5 5.5 0 0 0-7.78-7.77l-.61.61z"":
                        if (Output5 == null)
                        {
                            Output5 = Input;
                            Input = null;
                            Console.WriteLine($""{Output5.Text}号Box分配完毕,送往5号出口"");
                        }
                        break;
                    case ""M 9,2 11,7 17,7 12,10 14,15 9,12 4,15 6,10 1,7 7,7 Z"":
                        if (Output6 == null)
                        {
                            Output6 = Input;
                            Input = null;
                            Console.WriteLine($""{Output6.Text}号Box分配完毕,送往6号出口"");
                        }
                        break;
                    default:
                        if (Output7 == null)
                        {
                            Output7 = Input;
                            Input = null;
                            Console.WriteLine($""{Output7.Text}号Box分配完毕,送往7号出口"");
                        }
                        break;
                }
            }
        }
    }
}";
        }

    }
}
