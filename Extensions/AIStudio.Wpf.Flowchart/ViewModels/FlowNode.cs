﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using AIStudio.Wpf.DiagramDesigner;
using AIStudio.Wpf.DiagramDesigner.Models;
using AIStudio.Wpf.DiagramDesigner.Services;
using AIStudio.Wpf.DiagramDesigner.Serializable;
using AIStudio.Wpf.DiagramDesigner.Serializable.ViewModels;
using AIStudio.Wpf.Flowchart.Models;

namespace AIStudio.Wpf.Flowchart.ViewModels
{
    public class FlowNode : DiagramItemViewModel
    {
        protected IUIVisualizerService visualiserService;

        public FlowNode(NodeKinds kind) : this(null, kind)
        {
 
        }

        public FlowNode(IDiagramViewModel root, NodeKinds kind) : base(root)
        {
            Kind = kind;
            Text = Kind.GetDescription();           
        }

        public FlowNode(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public FlowNode(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new FlowNodeDesignerItem(this);
        }

        protected override void Init(IDiagramViewModel root, bool initNew)
        {
            base.Init(root, initNew);

            ShowRotate = false;
            ShowText = true;
            IsReadOnlyText = true;

            visualiserService = ApplicationServicesProvider.Instance.Provider.VisualizerService;
        }

        protected override void InitNew()
        {
            base.InitNew();
        }

        protected override void LoadDesignerItemViewModel(SelectableItemBase designerbase)
        {
            base.LoadDesignerItemViewModel(designerbase);

            if (designerbase is FlowNodeDesignerItem designer)
            {
                this.StatusColor = designer.Color;
                this.Kind = designer.Kind;
                this.StateImage = designer.StateImage;
                this.Status = designer.Status;
                this.Remark = designer.Remark;

                if (this is MiddleFlowNode middle)
                {
                    middle.UserIds = designer.UserIds;
                    middle.RoleIds = designer.RoleIds;
                    middle.ActType = designer.ActType;
                    middle.SimulateApprove = designer.SimulateApprove;                
                }
            }
        }

        public override DiagramNode ToDiagram()
        {
            var flowchartNode = new FlowchartNode();

            flowchartNode.StatusColor = StatusColor;
            flowchartNode.Kind = Kind;
            if (this is MiddleFlowNode middleflowNode)
            {
                flowchartNode.UserIds = middleflowNode.UserIds;
                flowchartNode.RoleIds = middleflowNode.RoleIds;
                flowchartNode.ActType = middleflowNode.ActType;
            }
            return flowchartNode;
        }

        private string _color = "#1890ff";
        [Browsable(false)]
        public string StatusColor
        {
            get { return _color; }
            set
            {
                SetProperty(ref _color, value);
            }
        } 

        [Browsable(false)]
        public NodeKinds Kind { get; set; }

        [Browsable(false)]
        public string StateImage { get; set; }

        #region 模拟使用
        private int _status;

        public int Status
        {
            get
            {
                return _status;
            }
            set
            {
                SetProperty(ref _status, value);
            }
        }

        private string _remark;
        public string Remark
        {
            get
            {
                return _remark;
            }
            set
            {
                SetProperty(ref _remark, value);
            }
        }
        #endregion

        #region 没有存起来，仅仅测试使用,实际这些代码应该都在服务端  
        public List<string> PreStepId { get; set; }
        public string NextStepId { get; set; }
        public Dictionary<string, string> SelectNextStep { get; set; } = new Dictionary<string, string>();
        #endregion

        public virtual Dictionary<string, string> PropertiesSetting
        {
            get
            {
                return new Dictionary<string, string>()
                {                   
                    { "Name","名称" },
                    { "Text","文本" },
                };
            }
        }
    }

    public class StartFlowNode : FlowNode
    {
        public StartFlowNode() : this(null)
        {

        }

        public StartFlowNode(IDiagramViewModel root) : base(root, NodeKinds.Start)
        {

        }

        public StartFlowNode(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public StartFlowNode(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }
    }

    public class EndFlowNode : FlowNode
    {
        public EndFlowNode() : this(null)
        {

        }

        public EndFlowNode(IDiagramViewModel root) : base(root, NodeKinds.End)
        {

        }

        public EndFlowNode(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public EndFlowNode(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }
    }

    public class DecideFlowNode : FlowNode
    {
        public DecideFlowNode() : this(null)
        {

        }

        public DecideFlowNode(IDiagramViewModel root) : base(root, NodeKinds.Decide)
        {

        }

        public DecideFlowNode(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public DecideFlowNode(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }
    }

    public class COBeginFlowNode : FlowNode
    {
        public COBeginFlowNode() : this(null)
        {

        }

        public COBeginFlowNode(IDiagramViewModel root) : base(root, NodeKinds.COBegin)
        {

        }

        public COBeginFlowNode(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public COBeginFlowNode(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }
    }

    public class COEndFlowNode : FlowNode
    {
        public COEndFlowNode() : this(null)
        {

        }

        public COEndFlowNode(IDiagramViewModel root) : base(root, NodeKinds.COEnd)
        {

        }

        public COEndFlowNode(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public COEndFlowNode(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }
    }
}
