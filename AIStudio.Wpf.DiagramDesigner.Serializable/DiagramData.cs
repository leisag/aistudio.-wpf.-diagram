﻿using System.Collections.Generic;
using AIStudio.Wpf.DiagramDesigner.Serializable.ViewModels;

namespace AIStudio.Wpf.DiagramDesigner.Serializable
{
    /// <summary>
    /// 
    /// </summary>
    public class DiagramData
    {
        /// <summary>
        /// Gets or sets the nodes.
        /// </summary>
        /// <value>
        /// The nodes.
        /// </value>
        public DiagramNode[] Nodes
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the links.
        /// </summary>
        /// <value>
        /// The links.
        /// </value>
        public DiagramLink[] Links
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the groups.
        /// </summary>
        /// <value>
        /// The groups.
        /// </value>
        public DiagramGroup[] Groups
        {
            get; set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DiagramData"/> class.
        /// </summary>
        public DiagramData()
        {
            Nodes = new DiagramNode[0];
            Links = new DiagramLink[0];
            Groups = new DiagramGroup[0];
        }
    }


    /// <summary>
    /// DiagramNode
    /// </summary>
    public class DiagramNode
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string Id
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the parent identifier.
        /// </summary>
        /// <value>
        /// The parent identifier.
        /// </value>
        public string ParentId
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        public string Color
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        public string LineColor
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        public string FontColor
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        public double FontSize
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        public string CornerRadius
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        public string BorderThickness
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the label.
        /// </summary>
        /// <value>
        /// The label.
        /// </value>
        public string Label
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public double Width
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public double Height
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the x.
        /// </summary>
        /// <value>
        /// The x.
        /// </value>
        public double X
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the y.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        public double Y
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the index of the z.
        /// </summary>
        /// <value>
        /// The index of the z.
        /// </value>
        public int ZIndex
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the port alignment list.
        /// </summary>
        /// <value>
        /// The port alignment list.
        /// </value>
        public List<string> PortAlignmentList
        {
            get; set;
        }

        public bool IsInnerConnector
        {
            get; set;
        }

        public List<string> PortRatio
        {
            get; set;
        }

        public virtual DiagramItemViewModel ToNodel(IDiagramViewModel diagramViewModel)
        {
            return new DiagramItemViewModel(diagramViewModel);
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class DiagramLink
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string Id
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        public string Color
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the color of the selected.
        /// </summary>
        /// <value>
        /// The color of the selected.
        /// </value>
        public string SelectedColor
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public double Width
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the label.
        /// </summary>
        /// <value>
        /// The label.
        /// </value>
        public string Label
        {
            get; set;
        }//TODO

        /// <summary>
        /// Gets or sets the source identifier.
        /// </summary>
        /// <value>
        /// The source identifier.
        /// </value>
        public string SourceId
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the target identifier.
        /// </summary>
        /// <value>
        /// The target identifier.
        /// </value>
        public string TargetId
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the source port alignment.
        /// </summary>
        /// <value>
        /// The source port alignment.
        /// </value>
        public string SourcePortAlignment
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the target port alignment.
        /// </summary>
        /// <value>
        /// The target port alignment.
        /// </value>
        public string TargetPortAlignment
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the index of the z.
        /// </summary>
        /// <value>
        /// The index of the z.
        /// </value>
        public int ZIndex
        {
            get; set;
        } = -1;

        /// <summary>
        /// Gets or sets the router.
        /// </summary>
        /// <value>
        /// The router.
        /// </value>
        public string Router
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the path generator.
        /// </summary>
        /// <value>
        /// The path generator.
        /// </value>
        public string PathGenerator
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the source marker path.
        /// </summary>
        /// <value>
        /// The source marker path.
        /// </value>
        public string SourceMarkerPath
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the width of the source marker.
        /// </summary>
        /// <value>
        /// The width of the source marker.
        /// </value>
        public double? SourceMarkerWidth
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the target marker path.
        /// </summary>
        /// <value>
        /// The target marker path.
        /// </value>
        public string TargetMarkerPath
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the width of the target marker.
        /// </summary>
        /// <value>
        /// The width of the target marker.
        /// </value>
        public double? TargetMarkerWidth
        {
            get; set;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DiagramGroup
    {
        /// <summary>
        /// Gets or sets the flow node ids.
        /// </summary>
        /// <value>
        /// The flow node ids.
        /// </value>
        public List<string> FlowNodeIds { get; set; } = new List<string>();
    }
}
